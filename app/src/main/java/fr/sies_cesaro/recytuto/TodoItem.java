package fr.sies_cesaro.recytuto;

import android.support.annotation.NonNull;

/**
 * Created by phil on 06/02/17.
 */

public class TodoItem implements Comparable<TodoItem>{

    @Override
    public int compareTo(@NonNull TodoItem o) {
        int res = 0;
        if(this.position < o.position){
            res = -1;
        }else{
            res = 1;
        }
        return res;
    }

    public enum Tags {
        Faible("Faible"), Normal("Normal"), Important("Important");

        private String desc;
        Tags(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }

    // Ajout de l'attribut id pour faciliter la suppression d'un item lors d'un click long
    private long id;
    private String label;
    private Tags tag;
    private boolean done;
    // L'attribut position permet de de définir la place de l'item dans la liste affichée à l'écran
    private int position;

    public TodoItem(Tags tag, String label, int pos) {
        this.tag = tag;
        this.label = label;
        this.id = -1;
        this.done = false;
        this.position = pos;
    }

    public TodoItem(String label, Tags tag, boolean done, int pos) {
        this.label = label;
        this.tag = tag;
        this.done = done;
        this.position = pos;
    }

    public static Tags getTagFor(String desc) {
        for (Tags tag : Tags.values()) {
            if (desc.compareTo(tag.getDesc()) == 0)
                return tag;
        }

        return Tags.Faible;
    }

    public String getLabel() {
        return label;
    }

    public Tags getTag() {
        return tag;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public void setTag(Tags tag) {
        this.tag = tag;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    // Getter et Setter pour l'attribut id
    public long getId() {return id; }

    public void setId(long id) {this.id = id; }
}
