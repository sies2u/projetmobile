package fr.sies_cesaro.recytuto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

import static fr.sies_cesaro.recytuto.TodoItem.Tags.Faible;
import static fr.sies_cesaro.recytuto.TodoItem.Tags.Important;
import static fr.sies_cesaro.recytuto.TodoItem.Tags.Normal;

public class ModificationItem extends AppCompatActivity {

    private TodoItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modification_item);

        Intent i = getIntent();
        // Récupération des données concernant l'item
        this.item = TodoDbHelper.getParticularItem((long)i.getSerializableExtra("item"),this);
        EditText lab = (EditText) findViewById(R.id.editText2);
        // Modification des vues pour afficher les données de l'item en cours
        lab.setText(this.item.getLabel());
        switch(this.item.getTag()){
            case Faible:
                ((RadioButton) findViewById(R.id.radioButton4)).setChecked(true);
                break;
            case Normal:
                ((RadioButton) findViewById(R.id.radioButton5)).setChecked(true);
                break;
            case Important:
                ((RadioButton) findViewById(R.id.radioButton6)).setChecked(true);
                break;
        }
    }

    // Méthode appelée lorsqu'on valide la modification de l'item
    public void modifierItem(View v){
        // Création de l'item avec les données fournies
        EditText lab = (EditText) findViewById(R.id.editText2);
        item.setLabel(lab.getText().toString());
        RadioButton r1 = (RadioButton) findViewById(R.id.radioButton4);
        RadioButton r2 = (RadioButton) findViewById(R.id.radioButton5);
        RadioButton r3 = (RadioButton) findViewById(R.id.radioButton6);
        if(r1.isChecked()){
            item.setTag(Faible);
        }else if(r2.isChecked()){
            item.setTag(Normal);
        }else if(r3.isChecked()){
            item.setTag(Important);
        }
        // Mise à jour de l'item
        TodoDbHelper.updateItem(item,this);
        setResult(RESULT_OK);
        finish();
    }

    // Méthode appelée lorsqu'on annule la modification
    public void annulerModification(View v){
        setResult(RESULT_CANCELED);
        finish();
    }
}
