package fr.sies_cesaro.recytuto;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.DragEvent;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    private ArrayList<TodoItem> items;
    private RecyclerView recycler;
    private LinearLayoutManager manager;
    private RecyclerAdapter adapter;

    private static final int CREATE_ITEM = 1;
    private static final int MODIFY_ITEM = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Ajout d'une page de création d'un item
        FloatingActionButton f = (FloatingActionButton) findViewById(R.id.fab);
        f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent creationItem = new Intent(getActivity(),CreationItem.class);
                startActivityForResult(creationItem, CREATE_ITEM);
            }
        });

        Log.i("INIT", "Fin initialisation composantes");

        // Ajout du listener sur la toolbar

        Toolbar tx = (Toolbar) findViewById(R.id.toolbar);
        tx.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch(item.getItemId()){
                    // dans le cas où l'item sélectionné est le bouton de debogage
                    case R.id.action_settings:
                        // On change de page pour afficher la bibliothèque de débogage
                        Intent dbmanager = new Intent(getActivity(), AndroidDatabaseManager.class);
                        startActivity(dbmanager);
                        break;
                    // dans le cas où l'item sélectionné est le bouton de suppression de la liste
                    case R.id.delete_settings:
                        // On lance la requête de suppression
                        TodoDbHelper.deleteItems(getActivity());
                        // On recharge la liste
                        items.removeAll(items);
                        recycler.removeAllViews();
                        break;
                }
                return false;
            }
        });

        // On récupère les items

        items = TodoDbHelper.getItems(getBaseContext());
        Collections.sort(items);
        Log.i("INIT", "Fin initialisation items");

        // On initialise le RecyclerView
        recycler = (RecyclerView) findViewById(R.id.recycler);
        manager = new LinearLayoutManager(this);
        recycler.setLayoutManager(manager);

        adapter = new RecyclerAdapter(items,this);
        recycler.setAdapter(adapter);

        // Ajout du listener OnItemTouchListener pour pouvoir supprimer un élément lors d'un click long
        recycler.addOnItemTouchListener(new RecyclerTouchListener(getActivity(),recycler,new ClickListener(){

            @Override
            public void onClick(View view, int position) {

            }

            // On redéfini la méthode onLongClick pour supprimer l'item sélectionné
            @Override
            public void onLongClick(View view,final int position) {
                // Lors d'un click long, affichage d'une boîte de dialogue
                new AlertDialog.Builder(getActivity())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Suppression d'un item")
                        .setMessage("Êtes-vous sûr de vouloir supprimer cet item ?")
                        // Si l'utilisateur dit oui, on supprime l'item et on recharge la page pour afficher les changements
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Suppression de l'item sélectionné
                                if(items.get(position)!=null) {
                                    TodoDbHelper.deleteParticularItem(items.get(position), getActivity());
                                    // On met à jour la liste
                                    items.remove(position);
                                    recycler.removeViewAt(position);
                                    adapter.notifyItemRemoved(position);
                                    adapter.notifyItemRangeChanged(position, items.size());
                                }
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        }));

        setRecyclerViewItemTouchListener();

        Log.i("INIT", "Fin initialisation recycler");
    }

    private Activity getActivity(){
        return this;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        switch(requestCode){
            case CREATE_ITEM:
                this.items = TodoDbHelper.getItems(getBaseContext());
                Collections.sort(items);
                this.adapter.notifyDataSetChanged();
                this.adapter = new RecyclerAdapter(items,this);
                this.recycler.setAdapter(this.adapter);
                setRecyclerViewItemTouchListener();
                break;
            case MODIFY_ITEM:
                this.items = TodoDbHelper.getItems(getBaseContext());
                Collections.sort(items);
                this.adapter.notifyDataSetChanged();
                this.adapter = new RecyclerAdapter(items,this);
                this.recycler.setAdapter(this.adapter);
                setRecyclerViewItemTouchListener();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setRecyclerViewItemTouchListener() {
        ItemTouchHelper.SimpleCallback itemTouchCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT){
            // OnMove permet de mettre en place le drag and drop
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder1) {
                // On swap la position entre les deux items
                Collections.swap(items, viewHolder.getAdapterPosition(), viewHolder1.getAdapterPosition());
                // On notifie l'adaptateur que certains items ont changé de position
                adapter.notifyItemMoved(viewHolder.getAdapterPosition(), viewHolder1.getAdapterPosition());
                // On met à jour la base de données
                TodoItem item = TodoDbHelper.getParticularItem(items.get(viewHolder.getAdapterPosition()).getId(),getActivity());
                TodoItem item2 = TodoDbHelper.getParticularItem(items.get(viewHolder1.getAdapterPosition()).getId(),getActivity());
                int pos = item.getPosition();
                item.setPosition(item2.getPosition());
                item2.setPosition(pos);
                TodoDbHelper.updateItem(item,getActivity());
                TodoDbHelper.updateItem(item2,getActivity());
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int position = viewHolder.getAdapterPosition();
                TodoItem item = items.get(position);

                switch(swipeDir) {
                    case ItemTouchHelper.RIGHT:
                        if(item.isDone()){
                            item.setDone(false);
                        }else{
                            item.setDone(true);
                        }
                        TodoDbHelper.updateItem(item,getActivity());
                        break;
                    case ItemTouchHelper.LEFT:
                        Intent modification = new Intent(getActivity(),ModificationItem.class);
                        modification.putExtra("item",item.getId());
                        startActivityForResult(modification,MODIFY_ITEM);
                        break;
                }
                recycler.getAdapter().notifyItemChanged(position);
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return false;
            }

            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                int swipeFlags = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
                return makeMovementFlags(dragFlags, swipeFlags);
            }

        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recycler);
        adapter.setItemTouchHelper(itemTouchHelper);
    }

    /*
     * Création d'une interface Click listener contenant les méthodes onClick et onLongClick avec View pour l'item et position dans la liste
     */
    public interface ClickListener{
        public void onClick(View view, int position);
        public void onLongClick(View view, int position);
    }

    /*
     * Création d'une classe RecyclerTouchListener implémentant l'interface OnItemTouchListener
     */
    public class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        private ClickListener clicklistener;
        private GestureDetector gestureDetector;


        // Constructeur qui initialise les paramètres
        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ClickListener clicklistener){

            this.clicklistener=clicklistener;
            // GestureDetector est utilisé pour écouter plusieurs types d'évenements (Touch events)
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }
                // On redéfini onLongPress pour appeler la méthode onLongClick de l'interface ClickListener
                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recycleView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clicklistener != null) {
                        clicklistener.onLongClick(child, recycleView.getChildAdapterPosition(child));
                    }
                }

            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child=rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                clicklistener.onClick(child,rv.getChildAdapterPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
