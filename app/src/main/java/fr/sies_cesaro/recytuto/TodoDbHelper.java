package fr.sies_cesaro.recytuto;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by phil on 11/02/17.
 */

public class TodoDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "todo.db";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TodoContract.TodoEntry.TABLE_NAME + " (" +
                    TodoContract.TodoEntry._ID + " INTEGER PRIMARY KEY," +
                    TodoContract.TodoEntry.COLUMN_NAME_LABEL + " TEXT," +
                    TodoContract.TodoEntry.COLUMN_NAME_TAG + " TEXT,"  +
                    TodoContract.TodoEntry.COLUMN_NAME_DONE +  " INTEGER," +
                    TodoContract.TodoEntry.COLUMN_NAME_POSITION +  " INTEGER)";

    public TodoDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Rien pour le moment
    }

    static ArrayList<TodoItem> getItems(Context context) {
        TodoDbHelper dbHelper = new TodoDbHelper(context);

        // Récupération de la base
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        // Création de la projection souhaitée
        String[] projection = {
                TodoContract.TodoEntry._ID,
                TodoContract.TodoEntry.COLUMN_NAME_LABEL,
                TodoContract.TodoEntry.COLUMN_NAME_TAG,
                TodoContract.TodoEntry.COLUMN_NAME_DONE,
                TodoContract.TodoEntry.COLUMN_NAME_POSITION
        };

        // Requête
        Cursor cursor = db.query(
                TodoContract.TodoEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );

        // Exploitation des résultats
        ArrayList<TodoItem> items = new ArrayList<TodoItem>();

        while (cursor.moveToNext()) {
            String label = cursor.getString(cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_LABEL));
            TodoItem.Tags tag = TodoItem.getTagFor(cursor.getString(cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_TAG)));
            boolean done = (cursor.getInt(cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_DONE)) == 1);
            int pos = (cursor.getInt(cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_POSITION)));
            TodoItem item = new TodoItem(label, tag, done, pos);

            // Ajout de l'id dans l'item créé
            item.setId(cursor.getLong(cursor.getColumnIndex(TodoContract.TodoEntry._ID)));
            items.add(item);
        }

         // Ménage
        dbHelper.close();

        // Retourne le résultat
        return items;
    }

    static void addItem(TodoItem item, Context context) {
        TodoDbHelper dbHelper = new TodoDbHelper(context);

        // Récupération de la base
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        // Récupération de la position la plus haute
        ArrayList<TodoItem> list = getItems(context);
        int maxPos = 0;
        if(list.size()>0) {
            maxPos = list.get(0).getPosition();
            for(int i = 1 ; i < list.size() ; i++){
                if(list.get(i).getPosition() > maxPos){
                    maxPos = list.get(i).getPosition();
                }
            }
            maxPos++;
        }
        // Création de l'enregistrement
        ContentValues values = new ContentValues();
        values.put(TodoContract.TodoEntry.COLUMN_NAME_LABEL, item.getLabel());
        values.put(TodoContract.TodoEntry.COLUMN_NAME_TAG, item.getTag().getDesc());
        values.put(TodoContract.TodoEntry.COLUMN_NAME_DONE, item.isDone());
        values.put(TodoContract.TodoEntry.COLUMN_NAME_POSITION,maxPos);
        // Enregistrement
        long newRowId = db.insert(TodoContract.TodoEntry.TABLE_NAME, null, values);

        // Ménage
        dbHelper.close();
    }

    /*
     * DeleteItems est la méthode utilisée pour supprimer tous les éléments de la liste
     */
    static void deleteItems(Context context) {
        TodoDbHelper dbHelper = new TodoDbHelper(context);

        // Récupération de la base
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        // Suppression
        db.delete(TodoContract.TodoEntry.TABLE_NAME,null,null);
        // Ménage
        dbHelper.close();
    }

    /*
     * DeleteItems est la méthode utilisée pour supprimer tous les éléments de la liste
     */
    static void deleteParticularItem(TodoItem item, Context context) {
        TodoDbHelper dbHelper = new TodoDbHelper(context);
        // Récupération de la base
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        // Suppression

        // On supprime l'item dont l'id est celui de l'item passé en paramètre
        db.delete(TodoContract.TodoEntry.TABLE_NAME,"_id=?", new String[]{""+item.getId()});

        // Ménage
        dbHelper.close();
    }

    /*
     * UpdateItem est une méthode qui sert à mettre à jour un item de la liste
     */
    static void updateItem(TodoItem item, Context context){
        TodoDbHelper dbHelper = new TodoDbHelper(context);
        // Récupération de la base
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        // mise à jour
        ContentValues obj = new ContentValues(4);
        obj.put("_id",item.getId());
        obj.put("label",item.getLabel());
        obj.put("tag",item.getTag()+"");
        obj.put("done",item.isDone());
        obj.put("position",item.getPosition());
        db.update(TodoContract.TodoEntry.TABLE_NAME, obj,"_id=?",new String[]{""+item.getId()});
    }

    /*
     * La méthode getParticularItem permet d'avoir un item selon son id
     */
    static TodoItem getParticularItem(long id,Context context){
        TodoDbHelper dbHelper = new TodoDbHelper(context);
        // Récupération de la base
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        // selection
        ArrayList<TodoItem> list = getItems(context);
        TodoItem t = null;
        // On récupère l'item choisi
        int i = 0;
        while(i<list.size()){
            if(list.get(i).getId()==id){
                t = list.get(i);
            }
            i++;
        }

        return t;
    }

    public ArrayList<Cursor> getData(String Query){
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[] { "message" };
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2= new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);

        try{
            String maxQuery = Query ;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);

            //add value to cursor2
            Cursor2.addRow(new Object[] { "Success" });

            alc.set(1,Cursor2);
            if (null != c && c.getCount() > 0) {

                alc.set(0,c);
                c.moveToFirst();

                return alc ;
            }
            return alc;
        } catch(SQLException sqlEx){
            Log.d("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+sqlEx.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        } catch(Exception ex){
            Log.d("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+ex.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        }
    }

}
