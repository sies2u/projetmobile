package fr.sies_cesaro.recytuto;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by phil on 07/02/17.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.TodoHolder> {

    private ArrayList<TodoItem> items;
    public static MainActivity m;
    public ItemTouchHelper itemTouchHelper;

    public RecyclerAdapter(ArrayList<TodoItem> items, MainActivity main) {
        this.items = items;
        this.m = main;
    }

    @Override
    public TodoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        return new TodoHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(final TodoHolder holder, int position) {
        TodoItem it = items.get(position);
        holder.bindTodo(it);
        // On ajoute le listener permettant le drag and drop à l'item
        if(it != null && holder != null && this.itemTouchHelper!=null) {
            ((TodoHolder) holder).layout2.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                        // Lorsque l'utilisateur touche l'item, on lance le drag
                        itemTouchHelper.startDrag(holder);
                    }
                    if(event.getActionMasked() == MotionEvent.ACTION_CANCEL){
                        Log.w("INIT","TESTESTE");
                    }
                    return false;
                }
            });
        }
    }

    public void setItemTouchHelper(ItemTouchHelper ith){
        this.itemTouchHelper = ith;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class TodoHolder extends RecyclerView.ViewHolder {
        private Resources resources;
        private ImageView image;
        private Switch sw;
        private TextView label;
        private LinearLayout layout2;
        private TodoItem item;
        private LinearLayout layout;

        public TodoHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.imageView);
            sw = (Switch) itemView.findViewById(R.id.switch1);
            label = (TextView) itemView.findViewById(R.id.textView);
            resources = itemView.getResources();
            layout = (LinearLayout) itemView.findViewById(R.id.layout);
            layout2 = (LinearLayout) itemView.findViewById(R.id.layout2);
            // On ajoute le listener permettant de mettre à jour la base de données lorsqu'on appuie sur le switch
            sw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // On change la couleur du switch
                    changerCouleurBackground();
                    // On modifie la valeur de l'item
                    if(sw.isChecked()){
                        item.setDone(true);
                    }else{
                        item.setDone(false);
                    }
                    // Puis on met à jour la base de données
                    TodoDbHelper.updateItem(item,m);
                }
            });

        }

        /*
         * ChangerCouleurBackground permet de changer la couleur en fonction du switch
         */
        public void changerCouleurBackground(){
            if(sw.isChecked()){
                layout.setBackgroundColor(resources.getColor(R.color.black));
            }else{
                layout.setBackgroundColor(resources.getColor(R.color.white));
            }
        }

        public void bindTodo(final TodoItem todo) {
            label.setText(todo.getLabel());
            this.item = todo;
            sw.setChecked(todo.isDone());
            changerCouleurBackground();
            switch(todo.getTag()) {
                case Faible:
                    image.setBackgroundColor(resources.getColor(R.color.faible));
                    break;
                case Normal:
                    image.setBackgroundColor(resources.getColor(R.color.normal));
                    break;
                case Important:
                    image.setBackgroundColor(resources.getColor(R.color.important));
                    break;
            }
        }

    }
}
