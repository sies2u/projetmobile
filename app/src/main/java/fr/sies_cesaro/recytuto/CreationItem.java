package fr.sies_cesaro.recytuto;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

import static fr.sies_cesaro.recytuto.TodoItem.Tags.Faible;
import static fr.sies_cesaro.recytuto.TodoItem.Tags.Important;
import static fr.sies_cesaro.recytuto.TodoItem.Tags.Normal;

public class CreationItem extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creation_item);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    // Méthode appelée lorsqu'on valide l'ajout
    public void validerAjout(View v){
        // Création de l'item avec les données fournies
        EditText lab = (EditText) findViewById(R.id.editText);
        String label = lab.getText().toString();
        TodoItem.Tags t = Faible;
        RadioButton r1 = (RadioButton) findViewById(R.id.radioButton);
        RadioButton r2 = (RadioButton) findViewById(R.id.radioButton2);
        RadioButton r3 = (RadioButton) findViewById(R.id.radioButton3);
        if(r1.isChecked()){
            t = Faible;
        }else if(r2.isChecked()){
            t = Normal;
        }else if(r3.isChecked()){
            t = Important;
        }
        // Mise à jour de la base de données
        TodoDbHelper.addItem(new TodoItem(label,t,false,0),this);
        setResult(RESULT_OK);
        finish();
    }

    // Méthode appelée lorsqu'on annule la création
    public void annulerAjout(View v){
        setResult(RESULT_CANCELED);
        finish();
    }

}
